package Sample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller("sample")
public class SampleController {

	@Autowired
	SampleService sampleService;
	
	
	@RequestMapping(value = "/test")
	public String home(){
		
		String strTemp = sampleService.SampleTest();
		
		System.out.println("ddd");
		
		return "index.html"; 
		
		
	}
	
}
