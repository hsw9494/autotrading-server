package Sample;


import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import common.utils.AbstractDAO;


@Repository
public class SampleDAO extends AbstractDAO {

    
	public List<LinkedHashMap<String, Object>> getTest() {        
        List<LinkedHashMap<String, Object>> aaa = selectList("mapper.sample.getTest");
        return aaa;
    }
	
}
